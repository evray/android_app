package com.example.evray;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.android_app.R;

import java.util.Locale;

public class ChangeLanguageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        String accessToken = getIntent().getStringExtra("accessToken");

        Button backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChangeLanguageActivity.this, HomeActivity.class);
                intent.putExtra("accessToken", accessToken);
                startActivity(intent);
            }
        });
    }

    public void goToLogin(View view) {
        String accessToken = getIntent().getStringExtra("accessToken");
        Locale locale = null;
        String language = "";

        switch (view.getId()) {
            case R.id.eesti_button:
                locale = new Locale("et");
                language = "Estonian";
                break;
            case R.id.english_button:
                locale = Locale.ENGLISH;
                language = "English";
                break;
            case R.id.russian_button:
                locale = new Locale("ru");
                language = "Russian";
                break;
        }

        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("LANGUAGE", language);

        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        intent.putExtra("accessToken", accessToken); // Pass the access token as an extra

        startActivity(intent);
    }

}
