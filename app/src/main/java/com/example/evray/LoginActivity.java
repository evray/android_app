package com.example.evray;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android_app.R;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    // Declare variables for the username, password, and login button views
    private EditText usernameEditText;
    private EditText passwordEditText;
    private Button loginButton;
    private String accessToken;


    private void setLocale(String languageCode) {
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);

        Configuration config = new Configuration();
        config.setLocale(locale);

        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        resources.updateConfiguration(config, dm);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Assign the username, password, and login button views to their respective variables
        usernameEditText = findViewById(R.id.username_edittext);
        passwordEditText = findViewById(R.id.password_edittext);
        loginButton = findViewById(R.id.login_button);

        loginButton.setText(getString(R.string.login));

        TextView resetButton = findViewById(R.id.forgot_password_button);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create an intent to launch the ResetPassword
                Intent intent = new Intent(LoginActivity.this, PasswordResetEnterEmailActivity.class);
                startActivity(intent);
            }
        });

        // Set an OnClickListener on the login button
        loginButton.setOnClickListener(view -> {
            // Get the values of the username and password EditText views
            String email = usernameEditText.getText().toString().trim();
            String password = passwordEditText.getText().toString().trim();

            // Check if the username an d password are not empty
            if (!email.isEmpty() && !password.isEmpty()) {
                // Call the login method with the username and password
                login(email, password);
            } else {
                // Display a toast message if the username or password is empty
                Toast.makeText(LoginActivity.this, getString(R.string.required_fields), Toast.LENGTH_LONG).show();
            }
        });

        Button backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void login(String username, String password) { // changed from email
        // Create an OkHttpClient to make the API request
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();

        // Create a JSON object containing the username and password
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", username);
            jsonObject.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Create a request body containing the JSON object
        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(jsonObject.toString(), mediaType);


        // Set the URL for the API request
        String API_URL = "http://tootaja.evray.ee/api/public/member/auth/login";
        Request request = new Request.Builder()
                .url(API_URL)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                // Called when the request fails to connect to the server
                runOnUiThread(() -> Toast.makeText(LoginActivity.this, getString(R.string.failed_to_connect), Toast.LENGTH_LONG).show());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                // Called when the server responds to the request
                assert response.body() != null;
                String responseBody = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(responseBody);
                    accessToken = jsonObject.getString("token");
                    // Do something with the access token, such as save it to SharedPreferences
                    runOnUiThread(() -> {
                        Toast.makeText(LoginActivity.this, getString(R.string.login_successful), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        intent.putExtra("accessToken", accessToken); // Pass the accessToken to the next activity
                        startActivity(intent);
                        finish();
                    });
                } catch (JSONException e) {
                    // Called if there is an error parsing the response JSON
                    e.printStackTrace();
                    runOnUiThread(() -> Toast.makeText(LoginActivity.this, getString(R.string.invalid_credentials), Toast.LENGTH_LONG).show());
                }
            }
        });
    }
}
