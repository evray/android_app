package com.example.evray;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.android_app.R;

public class HomeActivity extends AppCompatActivity {

    private Button logoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        String accessToken = getIntent().getStringExtra("accessToken");


        // find the logout button and set its onClickListener
        logoutButton = findViewById(R.id.logout_button);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create an intent to launch the LoginActivity
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);

                // Set the FLAG_ACTIVITY_CLEAR_TASK and FLAG_ACTIVITY_NEW_TASK flags
                // to clear the activity stack and create a new task for LoginActivity
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                // Start LoginActivity and finish HomeActivity
                startActivity(intent);
                finish();
            }
        });

        Button changePasswordButton = findViewById(R.id.change_password_button);
        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create an intent to launch the ChangePasswordActivity
                Intent intent = new Intent(HomeActivity.this, ChangePasswordActivity.class);
                intent.putExtra("accessToken", accessToken); // Pass the access token as an extra
                startActivity(intent);
            }
        });

        Button profileButton = findViewById(R.id.profile_page);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Handle the button click here
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                intent.putExtra("accessToken", accessToken); // Pass the access token as an extra
                startActivity(intent);
            }
        });

        // find the card_layout button and set its onClickListener
        Button cardLayoutButton = findViewById(R.id.card_layout_button);
        cardLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, CardLayoutActivity.class);
                intent.putExtra("accessToken", accessToken); // Pass the access token as an extra
                startActivity(intent);
            }
        });

        // find the card_layout button and set its onClickListener
        Button changeLanguageButton = findViewById(R.id.change_language);
        changeLanguageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, ChangeLanguageActivity.class);
                intent.putExtra("accessToken", accessToken); // Pass the access token as an extra
                startActivity(intent);
            }
        });
    }
}
