package com.example.evray;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.LocaleList;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android_app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CardLayoutActivity extends AppCompatActivity {

    private String accessToken;
    private String refreshToken;

    private TextView currentDateTextView;
    private TextView fullNameTextView;
    private TextView identificationCodeTextView;

    private static final String API_URL = "http://tootaja.evray.ee/api/private/member/card";

    private Handler handler;
    private Runnable updateTimeRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_card);

        Intent intent = getIntent();
        accessToken = intent.getStringExtra("accessToken");
        refreshToken = intent.getStringExtra("refreshToken");

        currentDateTextView = findViewById(R.id.current_date_textview);
        fullNameTextView = findViewById(R.id.firstname);
        identificationCodeTextView = findViewById(R.id.identification_code);

        // Create a Handler and a Runnable to update the time periodically
        handler = new Handler(Looper.getMainLooper());
        updateTimeRunnable = new Runnable() {
            @Override
            public void run() {
                updateCurrentDate();
                handler.postDelayed(this, 1000); // Update every second
            }
        };
        new CardDataTask().execute();

        Button backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CardLayoutActivity.this, HomeActivity.class);
                intent.putExtra("accessToken", accessToken);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.post(updateTimeRunnable); // Start updating the time when the activity resumes
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(updateTimeRunnable); // Stop updating the time when the activity pauses
    }

    private void updateCurrentDate() {
        // Set the current date and time
        Configuration config = getResources().getConfiguration();
        LocaleList locales = config.getLocales();

        Locale currentLocale;
        if (locales != null && !locales.isEmpty()) {
            currentLocale = locales.get(0);
        } else {
            currentLocale = Locale.getDefault();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss", currentLocale);
        String currentDate = dateFormat.format(new Date());
        currentDateTextView.setText(currentDate);
    }


    private class CardDataTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String result = null;

            try {
                URL url = new URL(API_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Authorization", "Bearer " + accessToken);

                int responseCode = connection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                    reader.close();
                    result = response.toString();
                } else if (responseCode == 498) {
                    // Token expired, attempt token refresh
                    result = refreshAccessTokenAndRetry();
                } else {
                    // Handle other error responses
                    result = "Error: " + responseCode;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject cardData = new JSONObject(result);

                String firstName = cardData.getString("firstname");
                String lastName = cardData.getString("lastname");

                String identificationCode = cardData.getString("identificationCode");

                fullNameTextView.setText(firstName + " " + lastName);
                identificationCodeTextView.setText(identificationCode);

                updateCurrentDate();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String refreshAccessTokenAndRetry() {
        String refreshedToken = null;

        // Perform token refresh using API /api/private/member/auth/refresh
        // Include the existing refresh token in the request body

        try {
            URL url = new URL("http://tootaja.evray.ee/api/private/member/auth/refresh");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            JSONObject requestBody = new JSONObject();
            requestBody.put("refresh_token", refreshToken); // Use the refresh token

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(requestBody.toString().getBytes());
            outputStream.flush();
            outputStream.close();

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
                reader.close();

                JSONObject responseJson = new JSONObject(response.toString());
                refreshedToken = responseJson.getString("access_token");

                // Update the access token with the refreshed token
                accessToken = refreshedToken;
            } else {
                // Handle token refresh failure
                refreshedToken = null;

                if (responseCode == 498) {
                    // Unauthorized, token refresh failed
                    // Log out the user and redirect to login page
                    logoutAndRedirectToLogin();
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return refreshedToken;
    }

    private void logoutAndRedirectToLogin() {
        // Perform logout actions here, such as clearing user data and session
        // Redirect the user to the login page
        Intent intent = new Intent(CardLayoutActivity.this, LoginActivity.class);
        startActivity(intent);
        finish(); // Optional: Finish the current activity to prevent going back to the profile page
    }
}
