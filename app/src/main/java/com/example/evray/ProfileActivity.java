package com.example.evray;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android_app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ProfileActivity extends AppCompatActivity {

    private String accessToken;
    private String refreshToken;

    private TextView fullNameTextView, professionTextView, companyNameTextView, identificationCodeTextView,
            emailTextView, dateOfBirthTextView, addressTextView, telephoneTextView;

    private static final String API_URL = "http://tootaja.evray.ee/api/private/member/profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Intent intent = getIntent();
        accessToken = intent.getStringExtra("accessToken");
        refreshToken = intent.getStringExtra("refreshToken");

        fullNameTextView = findViewById(R.id.full_name);
        professionTextView = findViewById(R.id.profession);
        companyNameTextView = findViewById(R.id.company_name);
        identificationCodeTextView = findViewById(R.id.identification_code);
        emailTextView = findViewById(R.id.email);
        dateOfBirthTextView = findViewById(R.id.date_of_birth);
        addressTextView = findViewById(R.id.address);
        telephoneTextView = findViewById(R.id.telephone);

        new ProfileDataTask().execute();

        Button editButton = findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
                intent.putExtra("accessToken", accessToken);
                intent.putExtra("email", emailTextView.getText().toString());
                intent.putExtra("address", addressTextView.getText().toString());
                intent.putExtra("telephone", telephoneTextView.getText().toString());
                startActivity(intent);
            }
        });

        Button backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
                intent.putExtra("accessToken", accessToken); // Pass the access token as an extra
                startActivity(intent);
            }
        });
    }

    private class ProfileDataTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String result = null;

            try {
                URL url = new URL(API_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Authorization", "Bearer " + accessToken);

                int responseCode = connection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                    reader.close();
                    result = response.toString();
                } else if (responseCode == 498) {
                    // Token expired, attempt token refresh
                    result = refreshAccessTokenAndRetry();
                } else {
                    // Handle other error responses
                    result = "Error: " + responseCode;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                try {
                    JSONObject profileData = new JSONObject(result);

                    String fullName = profileData.getString("firstname") + " " + profileData.getString("lastname");
                    String profession = profileData.getString("professionName");
                    String companyName = profileData.getString("companyName");
                    String identificationCode = profileData.getString("identificationCode");
                    String email = profileData.getString("email");
                    String dateOfBirth = profileData.getString("dateOfBirth");
                    String address = profileData.getString("address");
                    String telephone = profileData.getString("telephone");

                    fullNameTextView.setText(fullName);
                    professionTextView.setText(getString(R.string.profession) + " " + profession);
                    companyNameTextView.setText(getString(R.string.company_name) + " " + companyName);
                    identificationCodeTextView.setText(getString(R.string.identification_code_label) + " " + identificationCode);
                    emailTextView.setText(getString(R.string.email) + " " + email);
                    dateOfBirthTextView.setText(getString(R.string.date_of_birth) + " " + dateOfBirth);
                    addressTextView.setText(getString(R.string.address) + " " + address);
                    telephoneTextView.setText(getString(R.string.telephone) + " " + telephone);
                    addressTextView.setText(getString(R.string.address) + " " + address);
                    telephoneTextView.setText(getString(R.string.telephone) + " " + telephone);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
        Toast.makeText(ProfileActivity.this, getString(R.string.error_retrieving_profile_data), Toast.LENGTH_LONG).show();
            }
        }

        private String refreshAccessTokenAndRetry() {
            String refreshedToken = null;

            // Perform token refresh using API /api/private/member/auth/refresh
            // Include the existing refresh token in the request body

            try {
                URL url = new URL("http://tootaja.evray.ee/api/private/member/auth/refresh");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setDoOutput(true);

                JSONObject requestBody = new JSONObject();
                requestBody.put("refresh_token", refreshToken); // Use the refresh token

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(requestBody.toString().getBytes());
                outputStream.flush();
                outputStream.close();

                int responseCode = connection.getResponseCode();

                if (responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                    reader.close();

                    JSONObject responseJson = new JSONObject(response.toString());
                    refreshedToken = responseJson.getString("access_token");

                    // Update the access token with the refreshed token
                    accessToken = refreshedToken;
                } else {
                    // Handle token refresh failure
                    refreshedToken = null;

                    if (responseCode == 498) {
                        // Unauthorized, token refresh failed
                        // Log out the user and redirect to login page
                        logoutAndRedirectToLogin();
                    }
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            return refreshedToken;
        }
    }

    private void logoutAndRedirectToLogin() {
        // Perform logout actions here, such as clearing user data and session
        // Redirect the user to the login page
        Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
        startActivity(intent);
        finish(); // Optional: Finish the current activity to prevent going back to the profile page
    }
}
