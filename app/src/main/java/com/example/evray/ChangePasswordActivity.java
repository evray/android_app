package com.example.evray;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android_app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText mOldPasswordEditText;
    private EditText mNewPasswordEditText;
    private EditText mConfirmPasswordEditText;
    private String accessToken;
    private String refreshToken;


    private static final String API_URL = "http://tootaja.evray.ee/api/private/member/password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        mOldPasswordEditText = findViewById(R.id.old_password_edittext);
        mNewPasswordEditText = findViewById(R.id.new_password_edittext);
        mConfirmPasswordEditText = findViewById(R.id.confirm_password_edittext);
        Button mChangePasswordButton = findViewById(R.id.change_password_button);

        Intent intent = getIntent();
        accessToken = intent.getStringExtra("accessToken");
        refreshToken = intent.getStringExtra("refreshToken");


        mChangePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oldPassword = mOldPasswordEditText.getText().toString().trim();
                String newPassword = mNewPasswordEditText.getText().toString().trim();
                String confirmPassword = mConfirmPasswordEditText.getText().toString().trim();

                if (oldPassword.isEmpty() || newPassword.isEmpty() || confirmPassword.isEmpty()) {
                    Toast.makeText(ChangePasswordActivity.this, getString(R.string.please_fill_fields), Toast.LENGTH_LONG).show();
                } else if (!newPassword.equals(confirmPassword)) {
                    Toast.makeText(ChangePasswordActivity.this, getString(R.string.new_password_mismatch), Toast.LENGTH_LONG).show();
                } else {
                    // Call the AsyncTask to send the password change request
                    new ChangePasswordTask().execute(oldPassword, newPassword);
                }
            }
        });

        Button cancelButton = findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChangePasswordActivity.this, HomeActivity.class);
                intent.putExtra("accessToken", accessToken); // Pass the access token as an extra
                startActivity(intent);

            }
        });
    }

    private class ChangePasswordTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String oldPassword = params[0];
            String newPassword = params[1];

            String result = null;
            try {
                URL url = new URL(API_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("PATCH");
                connection.setRequestProperty("Authorization", "Bearer " + accessToken);
                connection.setRequestProperty("Content-Type", "application/json");

                JSONObject requestBody = new JSONObject();
                requestBody.put("oldPassword", oldPassword);
                requestBody.put("newPassword", newPassword);

                connection.setDoOutput(true);
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(requestBody.toString().getBytes());
                outputStream.flush();
                outputStream.close();

                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                    reader.close();
                    result = response.toString();
                } else if (responseCode == 498) {
                    // Token expired, attempt token refresh
                    result = refreshAccessTokenAndRetry();
                } else {
                    // Handle error response
                    result = "Error: " + responseCode;
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                result = "Error: " + e.getMessage();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (
                    result != null) {
                if (!result.startsWith("Error")) {
                // Password changed successfully
                    Toast.makeText(ChangePasswordActivity.this, getString(R.string.password_changed_success), Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    // Error changing password
                    Toast.makeText(ChangePasswordActivity.this, getString(R.string.error_changing_password) + result, Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(ChangePasswordActivity.this, getString(R.string.error_changing_password_generic), Toast.LENGTH_LONG).show();
            }
        }
    }

    private String refreshAccessTokenAndRetry() {
        String refreshedToken = null;

        // Perform token refresh using API /api/private/member/auth/refresh
        // Include the existing refresh token in the request body

        try {
            URL url = new URL("http://tootaja.evray.ee/api/private/member/auth/refresh");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            JSONObject requestBody = new JSONObject();
            requestBody.put("refresh_token", refreshToken); // Use the refresh token

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(requestBody.toString().getBytes());
            outputStream.flush();
            outputStream.close();

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
                reader.close();

                JSONObject responseJson = new JSONObject(response.toString());
                refreshedToken = responseJson.getString("access_token");

                // Update the access token with the refreshed token
                accessToken = refreshedToken;
            } else {
                // Handle token refresh failure
                refreshedToken = null;

                if (responseCode == 498) {
                    // Unauthorized, token refresh failed
                    // Log out the user and redirect to login page
                    logoutAndRedirectToLogin();
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return refreshedToken;
    }

    private void logoutAndRedirectToLogin() {
        // Perform logout actions here, such as clearing user data and session
        // Redirect the user to the login page
        Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
        startActivity(intent);
        finish(); // Optional: Finish the current activity to prevent going back to the profile page
    }
}