package com.example.evray;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import com.example.android_app.R;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToLogin(View view) {
        Locale locale = null;
        Intent intent = new Intent(this, LoginActivity.class);
        String language = "";

        switch (view.getId()) {
            case R.id.eesti_button:
                locale = new Locale("et");
                language = "Estonian";
                break;
            case R.id.english_button:
                locale = Locale.ENGLISH;
                language = "English";
                break;
            case R.id.russian_button:
                locale = new Locale("ru");
                language = "Russian";
                break;
        }

        intent.putExtra("LANGUAGE", language);
        startActivity(intent);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }
}
