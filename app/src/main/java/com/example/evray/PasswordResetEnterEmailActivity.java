package com.example.evray;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android_app.R;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PasswordResetEnterEmailActivity extends AppCompatActivity {
    private EditText emailEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_enter_email);

        emailEditText = findViewById(R.id.email_reset_password);

        // Retrieve the email value from the intent extras
        String email = getIntent().getStringExtra("email");
        if (email != null) {
            emailEditText.setText(email);
        }

        Button resetButton = findViewById(R.id.send_reset_code);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEditText.getText().toString().trim();
                resetPassword(email);
            }
        });

        Button iHaveCodeButton = findViewById(R.id.i_have_code_button);
        iHaveCodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEditText.getText().toString().trim();
                Intent intent = new Intent(getApplicationContext(), PasswordResetChangePasswordActivity.class);
                intent.putExtra("email", email); // Pass the email as an extra
                startActivity(intent);
            }
        });

        Button backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PasswordResetEnterEmailActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void resetPassword(String email) {
        // Create request body
        String requestBody = "{\"email\": \"" + email + "\"}";

        // Call the AsyncTask to send the POST request
        new ResetPasswordTask().execute(requestBody);
    }

    private class ResetPasswordTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            String requestBody = params[0];

            try {
                URL url = new URL("http://tootaja.evray.ee/api/public/member/password/reset");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setDoOutput(true);

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(requestBody.getBytes());
                outputStream.flush();
                outputStream.close();

                int responseCode = connection.getResponseCode();
                return responseCode == HttpURLConnection.HTTP_OK;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                Toast.makeText(PasswordResetEnterEmailActivity.this, getString(R.string.password_reset_email_sent), Toast.LENGTH_LONG).show();

                // Get the email
                String email = emailEditText.getText().toString().trim();

                // Create an intent to launch the next activity
                Intent intent = new Intent(PasswordResetEnterEmailActivity.this, PasswordResetChangePasswordActivity.class);
                intent.putExtra("email", email); // Pass the email as an extra
                startActivity(intent);
            } else {
                Toast.makeText(PasswordResetEnterEmailActivity.this, getString(R.string.failed_to_reset_password), Toast.LENGTH_LONG).show();
            }
        }
    }
}
