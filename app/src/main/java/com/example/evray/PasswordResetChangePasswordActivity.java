package com.example.evray;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android_app.R;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PasswordResetChangePasswordActivity extends AppCompatActivity {
    private EditText emailEditText;
    private EditText resetCodeEditText;
    private EditText newPasswordEditText;
    private EditText confirmPasswordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_enter_new_password);

        emailEditText = findViewById(R.id.email_reset_password);
        resetCodeEditText = findViewById(R.id.reset_code);
        newPasswordEditText = findViewById(R.id.new_password_edittext);
        confirmPasswordEditText = findViewById(R.id.confirm_password_edittext);

        // Retrieve the email value from the intent extras
        String email = getIntent().getStringExtra("email");
        if (email != null) {
            emailEditText.setText(email);
        }

        Button changePasswordButton = findViewById(R.id.change_password_button);
        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEditText.getText().toString().trim();
                String resetCode = resetCodeEditText.getText().toString().trim();
                String newPassword = newPasswordEditText.getText().toString().trim();
                String confirmPassword = confirmPasswordEditText.getText().toString().trim();

                if (!newPassword.equals(confirmPassword)) {
                    Toast.makeText(PasswordResetChangePasswordActivity.this, getString(R.string.passwords_do_not_match), Toast.LENGTH_LONG).show();
                } else {
                    changePassword(email, resetCode, newPassword);
                }
            }
        });

        Button backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PasswordResetChangePasswordActivity.this, PasswordResetEnterEmailActivity.class);
                startActivity(intent);
            }
        });
    }

    private void changePassword(String email, String resetCode, String newPassword) {
        String requestBody = "{\"email\": \"" + email + "\", \"code\": " + resetCode + ", \"newPassword\": \"" + newPassword + "\"}";
        new ChangePasswordTask().execute(requestBody);
    }

    private class ChangePasswordTask extends AsyncTask<String, Void, Integer> {
        @Override
        protected Integer doInBackground(String... params) {
            String requestBody = params[0];

            try {
                URL url = new URL("http://tootaja.evray.ee/api/public/member/password/reset/code");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setDoOutput(true);

                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(requestBody.getBytes());
                outputStream.flush();
                outputStream.close();

                int responseCode = connection.getResponseCode();
                return responseCode;
            } catch (IOException e) {
                e.printStackTrace();
                return -1;
            }
        }

        @Override
        protected void onPostExecute(Integer responseCode) {
            if (responseCode == 200) {
                Toast.makeText(PasswordResetChangePasswordActivity.this, getString(R.string.password_changed_success), Toast.LENGTH_LONG).show();
            } else if (responseCode == 403) {
                Toast.makeText(PasswordResetChangePasswordActivity.this, getString(R.string.wrong_reset_code), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(PasswordResetChangePasswordActivity.this, getString(R.string.failed_to_change_password), Toast.LENGTH_LONG).show();
            }
        }
    }
}
