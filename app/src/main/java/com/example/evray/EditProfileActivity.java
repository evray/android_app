package com.example.evray;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android_app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class EditProfileActivity extends AppCompatActivity {

    private String accessToken;
    private String refreshToken;
    private EditText emailEditText, addressEditText, telephoneEditText;

    private static final String API_URL = "http://tootaja.evray.ee/api/private/member/profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Intent intent = getIntent();
        accessToken = intent.getStringExtra("accessToken");
        refreshToken = intent.getStringExtra("refreshToken");

        emailEditText = findViewById(R.id.edit_email);
        addressEditText = findViewById(R.id.edit_address);
        telephoneEditText = findViewById(R.id.edit_telephone);

        // Retrieve the passed email, address, and telephone values
        String email = intent.getStringExtra("email");
        String address = intent.getStringExtra("address");
        String telephone = intent.getStringExtra("telephone");

        // Remove the labels from the retrieved values
        String emailText = email.replace(getString(R.string.email), "");
        String addressText = address.replace(getString(R.string.address), "");
        String telephoneText = telephone.replace(getString(R.string.telephone), "");

        // Set the retrieved values to the respective EditText fields
        emailEditText.setText(emailText);
        addressEditText.setText(addressText);
        telephoneEditText.setText(telephoneText);

        Button saveButton = findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailEditText.getText().toString().trim();
                String address = addressEditText.getText().toString().trim();
                String telephone = telephoneEditText.getText().toString().trim();

                // Check if the fields are empty and use the existing values if they are
                if (email.isEmpty()) {
                    email = emailText; // Use the existing value
                }

                if (address.isEmpty()) {
                    address = addressText; // Use the existing value
                }

                if (telephone.isEmpty()) {
                    telephone = telephoneText; // Use the existing value
                }

                // Check if the email is valid
                if (!isValidEmail(email)) {
                    Toast.makeText(EditProfileActivity.this, getString(R.string.invalid_email_address), Toast.LENGTH_LONG).show();
                    return; // Stop further execution
                }

                // Call the AsyncTask to send the PATCH request
                new UpdateProfileTask().execute(email, address, telephone);
            }
        });

        Button cancelButton = findViewById(R.id.cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class);
                intent.putExtra("accessToken", accessToken); // Pass the access token as an extra
                startActivity(intent);
            }
        });
    }

    private boolean isValidEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return email.matches(emailPattern);
    }

    private class UpdateProfileTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String email = params[0];
            String address = params[1];
            String telephone = params[2];

            String result = null;
            try {
                URL url = new URL(API_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("PATCH");
                connection.setRequestProperty("Authorization", "Bearer " + accessToken);
                connection.setRequestProperty("Content-Type", "application/json");

                JSONObject requestBody = new JSONObject();
                requestBody.put("email", email);
                requestBody.put("address", address);
                requestBody.put("telephone", telephone);

                connection.setDoOutput(true);
                OutputStream outputStream = connection.getOutputStream();
                outputStream.write(requestBody.toString().getBytes());
                outputStream.flush();
                outputStream.close();

                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder response = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.append(line);
                    }
                    reader.close();
                    result = response.toString();
                } else if (responseCode == 498) {
                    // Token expired, attempt token refresh
                    result = refreshAccessTokenAndRetry();
                } else {
                    // Handle error response
                    result = "Error: " + responseCode;
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
                result = "Error: " + e.getMessage();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result != null) {
                try {
                    JSONObject responseJson = new JSONObject(result);
                    JSONObject memberProfileDto = responseJson.getJSONObject("memberProfileDto");

                    String identificationCode = memberProfileDto.getString("identificationCode");
                    String email = memberProfileDto.getString("email");
                    String dateOfBirth = memberProfileDto.getString("dateOfBirth");
                    String dateOfEntry = memberProfileDto.getString("dateOfEntry");
                    String firstname = memberProfileDto.getString("firstname");
                    String lastname = memberProfileDto.getString("lastname");
                    String address = memberProfileDto.getString("address");
                    String telephone = memberProfileDto.getString("telephone");
                    String professionName = memberProfileDto.getString("professionName");
                    String companyName = memberProfileDto.getString("companyName");

                    // Update the UI with the updated profile information
                    // For example, you can use Toast to show the updated fields
                    Toast.makeText(EditProfileActivity.this, getString(R.string.profile_updated_success), Toast.LENGTH_LONG).show();

                    // You can also pass the updated data back to the ProfileActivity
                    Intent intent = new Intent(EditProfileActivity.this, ProfileActivity.class);
                    intent.putExtra("accessToken", accessToken);
                    intent.putExtra("email", email);
                    intent.putExtra("address", address);
                    intent.putExtra("telephone", telephone);
                    startActivity(intent);
                    finish(); // Close the current activity
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(EditProfileActivity.this, getString(R.string.error_updating_profile), Toast.LENGTH_LONG).show();
            }
        }
    }

    private String refreshAccessTokenAndRetry() {
        String refreshedToken = null;

        // Perform token refresh using API /api/private/member/auth/refresh
        // Include the existing refresh token in the request body

        try {
            URL url = new URL("http://tootaja.evray.ee/api/private/member/auth/refresh");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            JSONObject requestBody = new JSONObject();
            requestBody.put("refresh_token", refreshToken); // Use the refresh token

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(requestBody.toString().getBytes());
            outputStream.flush();
            outputStream.close();

            int responseCode = connection.getResponseCode();

            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder response = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
                reader.close();

                JSONObject responseJson = new JSONObject(response.toString());
                refreshedToken = responseJson.getString("access_token");

                // Update the access token with the refreshed token
                accessToken = refreshedToken;
            } else {
                // Handle token refresh failure
                refreshedToken = null;

                if (responseCode == 498) {
                    // Unauthorized, token refresh failed
                    // Log out the user and redirect to login page
                    logoutAndRedirectToLogin();
                }
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return refreshedToken;
    }

    private void logoutAndRedirectToLogin() {
        // Perform logout actions here, such as clearing user data and session
        // Redirect the user to the login page
        Intent intent = new Intent(EditProfileActivity.this, LoginActivity.class);
        startActivity(intent);
        finish(); // Optional: Finish the current activity to prevent going back to the profile page
    }
}